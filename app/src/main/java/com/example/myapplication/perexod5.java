package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class perexod5 extends Fragment {
TextView textView1,textView2, textView3, textView4, textView5;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_perexod5, container, false);
textView1 = v.findViewById(R.id.nastroyki);
textView2 = v.findViewById(R.id.profil);
textView3 = v.findViewById(R.id.ustroystva);
textView4 = v.findViewById(R.id.sintezator);
textView5 = v.findViewById(R.id.viyti);



        textView1.setMovementMethod(LinkMovementMethod.getInstance());
        textView2.setMovementMethod(LinkMovementMethod.getInstance());
        textView3.setMovementMethod(LinkMovementMethod.getInstance());
        textView4.setMovementMethod(LinkMovementMethod.getInstance());
        textView5.setMovementMethod(LinkMovementMethod.getInstance());
        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.show(((perexod3)getContext()).fragmentVoice).hide(((perexod3)getContext()).fragment3);
                ((perexod3)getContext()).active = ((perexod3)getContext()).fragmentVoice;
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        textView2.setOnClickListener(v1 -> {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.show(((perexod3)getContext()).fragmentProfile).hide(((perexod3)getContext()).fragment3);
            ((perexod3)getContext()).active = ((perexod3)getContext()).fragmentProfile;
            transaction.addToBackStack(null);
            transaction.commit();
        });
        textView3.setOnClickListener(v1 -> {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.show(((perexod3)getContext()).deviceSettings).hide(((perexod3)getContext()).fragment3);
            ((perexod3)getContext()).active = ((perexod3)getContext()).deviceSettings;
            transaction.addToBackStack(null);
            transaction.commit();
        });
        return v;
    }

}