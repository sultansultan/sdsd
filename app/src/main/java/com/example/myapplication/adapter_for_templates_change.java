package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class adapter_for_templates_change extends RecyclerView.Adapter<adapter_for_templates_change.ViewHolder> {
private List<recyclerview_row_for_templates_change>list_data;
private Context ct;

public adapter_for_templates_change(List<recyclerview_row_for_templates_change> list_data, Context ct) {
        this.list_data = list_data;
        this.ct = ct;
        }
@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_change_templates,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        recyclerview_row_for_templates_change ld=list_data.get(position);
holder.materialCardView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        ((perexod3)ct).showFileChooser();
    }
});
        }

@Override
public int getItemCount() {
        return list_data.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{

MaterialCardView materialCardView;
    perexod6 fragmentChange;
    public ViewHolder(View itemView) {
        super(itemView);
materialCardView = itemView.findViewById(R.id.cv_text_of_file);

fragmentChange = new perexod6();
    }
}
}