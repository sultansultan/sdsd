package com.example.myapplication;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentChange extends Fragment implements ClickInterface{
    public FragmentChange() {
    }
    private static final int FILE_SELECT_CODE = 0;

    public void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getContext(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    EditText edt_template;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change, container, false);
        View view = ((perexod3)getActivity()).findViewById(R.id.navigation);
        View view1 = view.findViewById(R.id.action_schedules);
        View view2 = view.findViewById(R.id.action_favorites);
        View view3 = view.findViewById(R.id.action_music);
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getFragmentManager().findFragmentByTag("4");
                if(fragment != null)
                    getFragmentManager().beginTransaction().hide(fragment).commit();

            }
        });
        edt_template = v.findViewById(R.id.edt_template);

        return v;
    }

    @Override
    public void foo() {

    }
}