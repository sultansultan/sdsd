package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class perexod3 extends AppCompatActivity {
   FragmentManager fm = getSupportFragmentManager();
    Fragment fragment1 = new FragmentTemplates();
    Fragment fragment2 = new perexod4();
    Fragment fragment3 = new perexod5();
    Fragment fragment4 = new perexod6();
    Fragment fragmentProfile = new FragmentProfile();
    Fragment fragmentVoice = new FragmentVoice();
    Fragment deviceSettings= new FragmentDeviceSettings();
    MaterialButton materialButton;
    Fragment active = fragment1;
    private static final int FILE_SELECT_CODE = 0;
    public void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getApplicationContext(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perexod3);

        BottomNavigationView navigation =   findViewById(R.id.navigation);
        fm.beginTransaction().add( R.id.main_container,deviceSettings, "6").hide(deviceSettings).commit();
        fm.beginTransaction().add( R.id.main_container,fragmentVoice, "6").hide(fragmentVoice).commit();
        fm.beginTransaction().add( R.id.main_container,fragmentProfile, "5").hide(fragmentProfile).commit();
        fm.beginTransaction().add( R.id.main_container,fragment4, "4").hide(fragment4).commit();
      fm.beginTransaction().add( R.id.main_container,fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.main_container, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.main_container,fragment1, "1").commit();
        navigation.setSelectedItemId(R.id.action_schedules);
        FloatingActionButton floatingActionButton =findViewById(R.id.fab_play);
        floatingActionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorText));
        floatingActionButton.setColorFilter(getResources().getColor(R.color.color_pressed));
   floatingActionButton.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           floatingActionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorText));
           floatingActionButton.setColorFilter(getResources().getColor(R.color.color_pressed));
           View view = navigation.findViewById(R.id.action_schedules);
           view.performClick();
       }
   });

        navigation.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_schedules:
                    fm.beginTransaction().hide(active).show(fragment1).commit();
                    floatingActionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorText));
                    floatingActionButton.setColorFilter(getResources().getColor(R.color.color_pressed));
                    active = fragment1;
                    return true;
                case R.id.action_favorites:
                    fm.beginTransaction().hide(active).show(fragment2).commit();
                    floatingActionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.color_normal));
                    floatingActionButton.setColorFilter(getResources().getColor(R.color.huynya));
                    active = fragment2;
                    return true;
              case R.id.action_music:
                    fm.beginTransaction().hide(active).show(fragment3).commit();
                  floatingActionButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.color_normal));
                  floatingActionButton.setColorFilter(getResources().getColor(R.color.huynya));
                    active = fragment3;
                    return true;
        //        case R.id.navhelp:
                    //   new KmConversationBuilder(MainActivity.this)
                    //           .setKmUser(user)
                    //          .launchConversation(new KmCallback() {
                    //           @Override
                    //          public void onSuccess(Object message) {

                    //             Log.d("Conversation", "Success : " + message);
                    //          }

                    //      @Override
                    //      public void onFailure(Object error) {
                    //           Log.d("Conversation", "Failure : " + error);
                    //         }
                    //      });
               //     fm.beginTransaction().hide(active).show(fragment4).commit();
                //    active = fragment4;
                  //  return true;
            }
            return false;

        });


    }}
