package com.example.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class
adapter_for_device_wifi extends RecyclerView.Adapter<adapter_for_device_wifi.ViewHolder> {
private List<recyclerview_row_for_change_wifi>list_data;
private Context ct;
public adapter_for_device_wifi(List<recyclerview_row_for_change_wifi> list_data, Context ct) {
        this.list_data = list_data;
        this.ct = ct;
        }
@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_change_wifi,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
    recyclerview_row_for_change_wifi ld=list_data.get(position);
    holder.textView.setText(ld.getName());
    holder.security.setText(ld.getDate());
    holder.urovensvyazi.setText(ld.getImgUrl());
holder.cardView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        holder.cardView.setStrokeWidth(3);
        holder.cardView.setStrokeColor(ContextCompat.getColor(ct, R.color.color_card_view_for_wifi));
        holder.textView.setTextColor(ContextCompat.getColor(ct, R.color.black));
        holder.cardView.invalidate();
        LayoutInflater inflater = (LayoutInflater)
                ct.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT ;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        MaterialButton yes = popupView.findViewById(R.id.mbtn_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            
            }
        });
        MaterialButton cancel = popupView.findViewById(R.id.mbtn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

            }
        });

        popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
    }
});

        }

@Override
public int getItemCount() {
        return list_data.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
MaterialCardView cardView;
TextView textView, urovensvyazi, security;
    public ViewHolder(View itemView) {
        super(itemView);
cardView = itemView.findViewById(R.id.mcv_click);
textView = itemView.findViewById(R.id.tv_1);
urovensvyazi = itemView.findViewById(R.id.tv_2);
security = itemView.findViewById(R.id.tv_3);

    }
}

}