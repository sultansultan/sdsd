package com.example.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
private List<recyclerview_row>list_data;
    boolean isPlaying = false;
private Context ct;
public MyAdapter(List<recyclerview_row> list_data, Context ct) {
        this.list_data = list_data;
        this.ct = ct;
        }
@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        recyclerview_row ld=list_data.get(position);
        holder.tv_music_name.setText(ld.getName());
        holder.tv_date.setText(ld.getDate());
holder.linearLayout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(holder.relativeLayout.getVisibility() == View.GONE){
holder.velt.setVisibility(View.VISIBLE);
            holder.relativeLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.velt.setVisibility(View.GONE);
            holder.relativeLayout.setVisibility(View.GONE);
        }
    }
});

    Picasso.get().load(ld.getImgVoiceUrl()).into(holder.img_voice);
if(ld.getImgVoiceUrl() == "https://i.ibb.co/891Nqkp/play.png") {
    holder.img_voice.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isPlaying) {
                Picasso.get()
                        .load(R.drawable.img_play)

                        .into(holder.img_voice);
            } else {
                Picasso.get()
                        .load(R.drawable.img_pause)

                        .into(holder.img_voice);
            }
            isPlaying = !isPlaying;
        }
    });
}

    Picasso.get()
        .load(ld.getImgUrl())

        .into(holder.img_file);

        }

@Override
public int getItemCount() {
        return list_data.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    private ImageView img_file;
    private TextView tv_music_name;
RelativeLayout relativeLayout ;
LinearLayout linearLayout;
View velt;
    private ImageView img_voice;
    private  TextView tv_date;


    public ViewHolder(View itemView) {
        super(itemView);
        relativeLayout = itemView.findViewById(R.id.rl_expand);
        velt = itemView.findViewById(R.id.view_view2);
        linearLayout = itemView.findViewById(R.id.ll_recyclerview_view);
        tv_date = itemView.findViewById(R.id.tv_date);
      img_voice = itemView.findViewById(R.id.img_play_or_stop);
        img_file=itemView.findViewById(R.id.img_music);
        tv_music_name=itemView.findViewById(R.id.tv_music_name);

    }
}
}