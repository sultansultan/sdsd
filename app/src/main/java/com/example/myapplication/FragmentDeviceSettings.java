package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;


public class FragmentDeviceSettings extends Fragment {

    private List<recyclerview_row_device_settings> list_data;
    private List<recyclerview_row_for_change_wifi> list_data_wifi;
    private RecyclerView recyclerView, recyclerView_wifi;
MaterialButton btn_search, btn_repeat;
RelativeLayout settings, search, repeat,found;
    private adapter_for_device_settings adapter;
    private adapter_for_device_wifi adapter_wifi;
    public FragmentDeviceSettings() {
        // Required empty public constructor
    }
    ImageView img_back_button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     View v =inflater.inflate(R.layout.fragment_device_settings, container, false);
        recyclerView=v.findViewById(R.id.recyclerview);
        recyclerView_wifi = v.findViewById(R.id.recyclerview_for_wifi);
        btn_search = v.findViewById(R.id.btn_search_device);
        settings = v.findViewById(R.id.rl_device_settings);
        repeat = v.findViewById(R.id.rl_not_found);
        btn_repeat = v.findViewById(R.id.btn_repeat_search);

        search = v.findViewById(R.id.rl_device_search);
        found = v.findViewById(R.id.rl_found);
        btn_repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
repeat.setVisibility(View.GONE);
found.setVisibility(View.VISIBLE);
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings.setVisibility(View.GONE);
                search.setVisibility(View.VISIBLE);
                new CountDownTimer(3000, 1000) {
                    public void onTick(long millisUntilFinished) {

                    }

                    public void onFinish() {
                        search.setVisibility(View.GONE);
                        repeat.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        });
        img_back_button = v.findViewById(R.id.img_back);

        img_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list_data=new ArrayList<>();
        recyclerview_row_device_settings listData = new recyclerview_row_device_settings();


        list_data.add(listData);


        adapter=new adapter_for_device_settings(list_data,getActivity());
        recyclerView.setAdapter(adapter);

        recyclerView_wifi.setHasFixedSize(true);
        recyclerView_wifi.setLayoutManager(new LinearLayoutManager(getContext()));
        list_data_wifi=new ArrayList<>();
        recyclerview_row_for_change_wifi listData_wifi = new recyclerview_row_for_change_wifi("Local-1", "Уровень сигнала: -10", "Безопасность: ???");
        recyclerview_row_for_change_wifi listData_wifi1 = new recyclerview_row_for_change_wifi("Local-2", "Уровень сигнала: -20", "Безопасность: ???");


        list_data_wifi.add(listData_wifi);
        list_data_wifi.add(listData_wifi1);


        adapter_wifi=new adapter_for_device_wifi(list_data_wifi,getActivity());

        recyclerView_wifi.setAdapter(adapter_wifi);
        return v;
    }


}