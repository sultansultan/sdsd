package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class perexod4 extends Fragment implements ClickInterface{
    private List<recyclerview_row_for_templates> list_data;
    private RecyclerView recyclerView;
    private static final int FILE_SELECT_CODE = 0;

    public void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getContext(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    private adapter_for_templates adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_perexod4, container, false);
        recyclerView=v.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list_data=new ArrayList<>();
        ImageView plus_add_button = v.findViewById(R.id.plussss);
        plus_add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.show(((perexod3)getContext()).fragment4).hide(((perexod3)getContext()).fragment2);
                ((perexod3)getContext()).active = ((perexod3)getContext()).fragment4;
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        recyclerview_row_for_templates listData = new recyclerview_row_for_templates("Сказки, песни", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png" );
        recyclerview_row_for_templates listData1 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData2 = new recyclerview_row_for_templates("Поведение за столом", currentDateandTime, "https://i.ibb.co/QfZ8Dn3/Group-3-2.png");
        recyclerview_row_for_templates listData3 = new recyclerview_row_for_templates("Песня Енота", currentDateandTime, "https://i.ibb.co/QfZ8Dn3/Group-3-2.png");
        recyclerview_row_for_templates listData4 = new recyclerview_row_for_templates("Теремок", currentDateandTime, "https://i.ibb.co/CKRt64d/Group-4-12.png");
        recyclerview_row_for_templates listData5 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData6 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData7 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData8 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData9 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData10 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData11 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData12 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData13 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        recyclerview_row_for_templates listData14 = new recyclerview_row_for_templates("обучение", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png");
        list_data.add(listData);
        list_data.add(listData1);
        list_data.add(listData2);
        list_data.add(listData3);
        list_data.add(listData4);
        list_data.add(listData5);
        list_data.add(listData6);
        list_data.add(listData7);
        list_data.add(listData8);
        list_data.add(listData9);
        list_data.add(listData10);
        list_data.add(listData11);
        list_data.add(listData12);
        list_data.add(listData13);
        list_data.add(listData14);
        adapter=new adapter_for_templates(list_data,getActivity(), this);
        recyclerView.setAdapter(adapter);


return v;
    }

    @Override
    public void foo() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.show(((perexod3)getContext()).fragment4).hide(((perexod3)getContext()).fragment2);
        ((perexod3)getContext()).active = ((perexod3)getContext()).fragment4;
        transaction.addToBackStack(null);
        transaction.commit();

    }
}