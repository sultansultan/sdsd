package com.example.myapplication;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class FragmentTemplates extends Fragment {
    public FragmentTemplates() {
        // Required empty public constructor
    }
    String[] bankNames={"BOI","SBI","HDFC","PNB","OBC"};
    private List<recyclerview_row> list_data;
    private RecyclerView recyclerView;

    private MyAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
View v = inflater.inflate(R.layout.fragment_templates, container, false);

        recyclerView=v.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list_data=new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        recyclerview_row listData = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData1 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData2 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");

        recyclerview_row listData3 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");

        recyclerview_row listData4 = new recyclerview_row("Сказки на ночь", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/QfZ8Dn3/Group-3-2.png");

        recyclerview_row listData5 = new recyclerview_row("поведение за столом", currentDateandTime, "https://i.ibb.co/QfZ8Dn3/Group-3-2.png",  "https://i.ibb.co/mb4WtKD/Group-6.png");

        recyclerview_row listData6 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData7 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData8 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData9 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData10 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData11 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");
        recyclerview_row listData12 = new recyclerview_row("волк и лиса", currentDateandTime, "https://i.ibb.co/xYBB9Pf/Group-3.png",  "https://i.ibb.co/891Nqkp/play.png");

        list_data.add(listData);
        list_data.add(listData1);
        list_data.add(listData2);
        list_data.add(listData3);
        list_data.add(listData4);
        list_data.add(listData5);
        list_data.add(listData6);
        list_data.add(listData7);
        list_data.add(listData8);
        list_data.add(listData9);
        list_data.add(listData10);
        list_data.add(listData11);
        list_data.add(listData12);
        adapter=new MyAdapter(list_data,getActivity());
        recyclerView.setAdapter(adapter);
        Spinner spin = (Spinner) v.findViewById(R.id.nice_spinner);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

//Creating the ArrayAdapter instance having the bank name list
        ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,bankNames);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);
        return v;
    }

}