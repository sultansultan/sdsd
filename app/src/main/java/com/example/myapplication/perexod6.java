package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class perexod6 extends Fragment {
    private List<recyclerview_row_for_templates_change> list_data;
    private RecyclerView recyclerView;

    private adapter_for_templates_change adapter;
ImageView img_back_button, img_plus_button;
    private static final int FILE_SELECT_CODE = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_perexod6, container, false);
img_back_button = v.findViewById(R.id.img_back);
img_plus_button = v.findViewById(R.id.img_change_plus_icon);
img_back_button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        getActivity().onBackPressed();
    }
});
        recyclerView=v.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list_data=new ArrayList<>();
        recyclerview_row_for_templates_change listData = new recyclerview_row_for_templates_change();


        list_data.add(listData);


        adapter=new adapter_for_templates_change(list_data,getActivity());
        recyclerView.setAdapter(adapter);
        return v;
    }
}