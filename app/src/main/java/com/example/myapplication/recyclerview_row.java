package com.example.myapplication;
public class recyclerview_row {
    private String mName;
    private String mImgUrl;
    private String mdate;


    private String mImgVoiceUrl;
    public recyclerview_row(){

    }
    public recyclerview_row(String name,  String date,String imgUrl, String imgVoiceUrl){
        if (name.trim().equals("")){
            name="No Name";
        }
        mName=name;
        mImgUrl=imgUrl;
        mdate = date;
        mImgVoiceUrl = imgVoiceUrl;
    }

    public String getName() {
        return mName;
    }
    public String getDate() {
        return mdate;
    }
    public void setDate(String date) {
        mdate=date;
    }
    public void setName(String name) {
        mName=name;
    }

    public String getImgUrl() {
        return mImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl=imgUrl;
    }
    public String getImgVoiceUrl() {
        return mImgVoiceUrl;
    }
    public void setImgVoiceUrl(String imgVoiceUrl) {
        mImgVoiceUrl=imgVoiceUrl;
    }

}