package com.example.myapplication;
public class recyclerview_row_for_templates_change {
    private String mName;
    private String mImgUrl;
    private String mdate;

    public recyclerview_row_for_templates_change(){

    }
    public recyclerview_row_for_templates_change(String name, String date, String imgUrl){
        if (name.trim().equals("")){
            name="No Name";
        }
        mName=name;
        mImgUrl=imgUrl;
        mdate = date;

    }

    public String getName() {
        return mName;
    }
    public String getDate() {
        return mdate;
    }
    public void setDate(String date) {
        mdate=date;
    }
    public void setName(String name) {
        mName=name;
    }

    public String getImgUrl() {
        return mImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl=imgUrl;
    }


}