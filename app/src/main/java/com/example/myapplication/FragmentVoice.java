package com.example.myapplication;

import android.os.Bundle;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentVoice extends Fragment {


    public FragmentVoice() {
        // Required empty public constructor
    }
TextView tv_language, tv_sintezator, tv_voice;
ImageView img_back_button;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
View v =inflater.inflate(R.layout.fragment_voice, container, false);
        img_back_button = v.findViewById(R.id.img_back);
        tv_sintezator = v.findViewById(R.id.tv_sintezator);
        tv_voice= v.findViewById(R.id.tv_voice);
        tv_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), tv_voice);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_voice, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_voice.setText(item.getTitle());
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
        tv_sintezator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), tv_sintezator);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_sintezator, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_sintezator.setText(item.getTitle());
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
        img_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        tv_language = v.findViewById(R.id.tv_language);
        tv_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(getContext(), tv_language);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        tv_language.setText(item.getTitle());
                        return true;
                    }
                });

                popup.show();//showing popup menu

        }
        });
        return v;
    }
}