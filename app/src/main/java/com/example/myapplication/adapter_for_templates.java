package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.List;

public class adapter_for_templates extends RecyclerView.Adapter<adapter_for_templates.ViewHolder> {
private List<recyclerview_row_for_templates>list_data;
private Context ct;
ClickInterface listener;

public adapter_for_templates(List<recyclerview_row_for_templates> list_data, Context ct, ClickInterface listener) {
        this.list_data = list_data;
        this.ct = ct;
        this.listener = listener;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_for_templates,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        recyclerview_row_for_templates ld=list_data.get(position);
        holder.tv_music_name.setText(ld.getName());
        holder.tv_date.setText(ld.getDate());
holder.linearLayout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(holder.relativeLayout.getVisibility() == View.GONE){
holder.velt.setVisibility(View.VISIBLE);
            holder.relativeLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.relativeLayout.setVisibility(View.GONE);
            holder.velt.setVisibility(View.GONE);
        }
    }
});

    holder.btn_change.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            listener.foo();
        }

    });



    Picasso.get()
        .load(ld.getImgUrl())

        .into(holder.img_file);

        }

@Override
public int getItemCount() {
        return list_data.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    private ImageView img_file;
    private TextView tv_music_name;



    MaterialButton btn_change;
RelativeLayout relativeLayout;
View velt;

LinearLayout linearLayout;
    private  TextView tv_date;
    public ViewHolder(View itemView) {
        super(itemView);
        relativeLayout = itemView.findViewById(R.id.rl_expand);
        linearLayout = itemView.findViewById(R.id.ll_recyclerview_view_for_templates);
        tv_date = itemView.findViewById(R.id.tv_date);

        velt = itemView.findViewById(R.id.view_view2);
        btn_change = itemView.findViewById(R.id.btn_repeat);
        img_file=itemView.findViewById(R.id.img_music);
        tv_music_name=itemView.findViewById(R.id.tv_music_name);

    }
}
}