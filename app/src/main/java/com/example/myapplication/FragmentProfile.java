package com.example.myapplication;

import android.media.Image;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentProfile extends Fragment {
    public FragmentProfile() {
        // Required empty public constructor
    }
ImageView  img_back_button;
CircleImageView img_profile_photo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
img_profile_photo = v.findViewById(R.id.profile_image);
        img_back_button = v.findViewById(R.id.img_back);

        img_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

Picasso.get().load(R.drawable.img_profile_photo_placeholder).placeholder(R.drawable.img_profile_photo_placeholder).into(img_profile_photo);
        return v;
    }
}